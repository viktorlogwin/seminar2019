import uuidv4 from 'uuid/v4';

before(() => {
    cy.visit('http://127.0.0.1:5500/index.html')
});

describe('Start page test', () => {

    it('Finds a logo', () => {
        cy.get('img').should('have.attr', 'src').should('include', 'cookbook-logo');
    });

    it('Finds the text visible for non registered user on the page', function () {
        cy.get('h5').should('contain', 'Fehler');
    });

    it('Finds two visible navigation menu items', function () {
        cy.get('li.logged-out').should('have.length', 2);
    });
});

describe('Failed and successfully Sign up test', () => {
    it('Failed signs up a new user', () => {

        // Data fixture
        const email1 = 'probetest.project@gmail.com';
        const pass1 = '123456';

        // Click Sign up
        cy.get('a:contains("Sign up")').click();

        // Fill out the form
        cy.get('#signup-email').type(email1);
        cy.get('#signup-password').type(pass1);
        cy.get('#signup-form > button').click();

        // Catch an error
        cy.get('p.error').contains('The email address is already in use by another account.');

    });

    it('Successfully signs up a new user', () => {

        // Data fixture
        const email2 = uuidv4() + '@gmail.com';
        const pass2 = '123456';

        //Clear the input form
        cy.get('#signup-email').clear();
        cy.get('#signup-password').clear();

        // Fill out the form
        cy.get('#signup-email').type(email2);
        cy.get('#signup-password').type(pass2);
        cy.get('#signup-form > button').click();

        //Finds three visible navigations menu
        cy.get('li.logged-in').should('have.length', 3);
    });

    it('Logged out', () => {
        //Logged out
        cy.get('a:contains("Logout")').click();
        cy.get('li.logged-out').should('have.length', 2);
    });
});

describe('Logged in and sucsessfuly created a new Recipe', () => {
    it('Logged in', () => {
        // Data fixture
        const email1 = 'probetest.project@gmail.com';
        const pass1 = '123456';

        // Click Login
        cy.get('a:contains("Login")').click();

        // Fill out the form
        cy.get('#login-email').type(email1);
        cy.get('#login-password').type(pass1);
        cy.get('#login-form > button').click();

        //Finds three visible navigations menu
        cy.get('li.logged-in').should('have.length', 3);

    });

    it('Creates recipe', () => {
        const name = 'Tiramisu';
        const ingredients = '250 g Mascarpone, 250 g Löffelbiskuits, 2 Eier, 60 g Puderzucker, 100 ml  Espresso, 3 EL  Amaretto, 1 EL Schokolade (gerieben), 1 EL Kakaopulver, Salz';
        const cooking = 'Die Eier trennen. Die Eigelbe mit Puderzucker in eine Schüssel geben und cremig schlagen. Anschließend mit Amaretto und der Mascarpone glatt rühren.';

        //Opened a recipe creation menu
        cy.get('a:contains("Create Recipe")').click();

        // Fill out the form
        cy.get('h4').should('contain', 'Create Recipe');
        cy.get('#title').type(name);
        cy.get('#content').type(ingredients);
        cy.get('#direction').type(cooking);
        cy.get('#create-form > button').click();

        // Finds recipe
        cy.get('div.collapsible-header:contains("Tiramisu")').click();
        cy.get('li.active').contains(ingredients);
        cy.get('li.active').contains(cooking);
    });

    it('Logged out', () => {
        //Logged out
        cy.get('a:contains("Logout")').click();
        cy.get('li.logged-out').should('have.length', 2);
    });
});


