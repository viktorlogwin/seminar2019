const puppeteer = require('puppeteer');
const expect = require('chai').expect;
const uuidv4 = require('uuid/v4');

let browser;
let page;

before(async () => {
    console.log('before');
    browser = await puppeteer.launch({ headless: false });
    page = await browser.newPage();
    await page.goto('http://127.0.0.1:5500/index.html', { waitUntil: 'networkidle2' });
});

describe('Start page test', () => {

    it('Finds a logo', async () => {
        const src = await page.$eval('img', el => el.src);
        expect(src).to.include('cookbook-logo');
    });

    it('Finds the text visible for non registered user', async () => {
        const text = await page.$eval('h5', el => el.textContent);
        expect(text).to.include('Fehler');
    });

    it('Finds two visible navigation menu items', async () => {
        const li = await page.$$('li.logged-out');
        expect(li).to.have.lengthOf(2);
    });
});

describe('Failed and successfully Sign up test', () => {
    it('Failed signs up a new user', async () => {

        // Data fixture
        const email1 = 'probetest.project@gmail.com';
        const pass1 = '123456';

        // Click Sign up
        await page.click('a[data-target="modal-signup"]');

        await page.waitFor(2000);

        // Fill out the form
        await page.type('#signup-email', email1);
        await page.type('#signup-password', pass1);
        await page.click('#signup-form > button');

        await page.waitFor(2000);

        // Catch an error
        const text = await page.$eval('p.error', el => el.textContent);
        expect(text).to.include('The email address is already in use by another account.');
    });

    it('Successfully signs up a new user', async () => {

        // Data fixture
        const email2 = uuidv4() + '@gmail.com';
        const pass2 = '123456';

        //Clear the input form
        await page.$eval('#signup-email', el => el.value = '');
        await page.$eval('#signup-password', el => el.value = '');

        // Fill out the form
        await page.type('#signup-email', email2);
        await page.type('#signup-password', pass2);
        await page.click('#signup-form > button');

        await page.waitFor(2000);

        //Finds three visible navigations menu
        const li = await page.$$('li.logged-in');
        expect(li).to.have.lengthOf(3);
    });

    it('Logged out', async () => {
        //Logged out
        await page.waitFor(2000);

        await page.click('#logout');

        await page.waitFor(2000);

        const li = await page.$$('li.logged-out');
        expect(li).to.have.lengthOf(2);
    });
});

describe('Logged in and sucsessfuly created a new recipe', () => {
    it('Logged in', async () => {
        // Data fixture
        const email1 = 'probetest.project@gmail.com';
        const pass1 = '123456';

        // Click Login
        await page.click('a[data-target="modal-login"]');

        await page.waitFor(2000);

        // Fill out the form
        await page.type('#login-email', email1);
        await page.type('#login-password', pass1);
        await page.click('#login-form > button');

        await page.waitFor(2000);

        //Finds three visible navigations menu
        const li = await page.$$('li.logged-in');
        expect(li).to.have.lengthOf(3);
    });

    it('Creates recipe', async () => {
        const name = 'Marmorkuchen';
        const ingredients = '250g weiche Butter, 200g Zucker, Vanillzucker, Salz, 5 Eier, 250g Mehl, 3 TL Backpulver, 3 EL Milch, 2 EL Backkakao, 1 EL Zucker, 50ml Milch';
        const cooking = 'Weiche Butter mit Zucker, Vanillinzucker und Salz schaumig schlagen. Nach und nach die Eier zugeben. Mehl mit Backpulver mischen und abwechselnd mit der Milch unterrühren.';

        await page.waitFor(2000);

        //Opened a recipe creation menu
        await page.click('a[data-target="modal-create"]');

        await page.waitFor(2000);

        // Fill out the form
        let text = await page.$eval('#modal-create > div > h4', el => el.textContent);
        expect(text).to.include('Create Recipe');
        await page.type('#title', name);
        await page.type('#content', ingredients);
        await page.type('#direction', cooking);
        await page.click('#create-form > button');

        await page.waitFor(2000);

        // Finds recipe
        text = await page.$$eval('div.collapsible-header', el => el.find(e => e.innerText == 'Marmorkuchen').click());

        let li = await page.$eval('li.active > div#recipeContent', el => el.innerText);
        expect(li).to.include(ingredients);
        li = await page.$eval('li.active > div#recipeDirection', el => el.innerText);
        expect(li).to.include(cooking);
    });

    it('Logged out', async () => {
        //Logged out
        await page.waitFor(2000);

        await page.click('#logout');

        await page.waitFor(2000);

        const li = await page.$$('li.logged-out');
        expect(li).to.have.lengthOf(2);
    });
});

after(async () => {
    console.log('close');
    await browser.close();
});