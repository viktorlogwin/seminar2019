// DOM elements
const rezepteList = document.querySelector('.rezepte');
const loggedOutLinks = document.querySelectorAll('.logged-out');
const loggedInLinks = document.querySelectorAll('.logged-in');
const accountDetails = document.querySelector('.account-details');

const setupUI = (user) => {
    if (user) {
        // account info
        const html = `
          <div>Logged in as ${user.email}</div>
        `;
        accountDetails.innerHTML = html;
        // toggle user UI elements
        loggedInLinks.forEach(item => item.style.display = 'block');
        loggedOutLinks.forEach(item => item.style.display = 'none');
    } else {
         // clear account info
        accountDetails.innerHTML = '';
        // toggle user elements
        loggedInLinks.forEach(item => item.style.display = 'none');
        loggedOutLinks.forEach(item => item.style.display = 'block');
    }
};

// setup rezepte
const setupRezepte = (data) => {

    if (data.length) {
        let html = '';
        data.forEach(doc => {
            const rezepte = doc.data();
            const li = `
      <li>
        <div id="recipeHeader" class="collapsible-header grey lighten-4"> ${rezepte.title} </div>
        <div id="recipeContent" class="collapsible-body white"> ${rezepte.content} </div>
        <div id="recipeDirection" class="collapsible-body white"> ${rezepte.direction} </div>
      </li>
    `;
            html += li;
        });
        rezepteList.innerHTML = html;
    } else {
        rezepteList.innerHTML = '<h5 class="center-align">Login to view Recipes</h5>';
    }
};


// setup materialize components
document.addEventListener('DOMContentLoaded', function () {

    var modals = document.querySelectorAll('.modal');
    M.Modal.init(modals);

    var items = document.querySelectorAll('.collapsible');
    M.Collapsible.init(items);

});