import { Selector } from 'testcafe';
import { fixture, test } from '../testcafe-defs';
const uuidv4 = require('uuid/v4');

fixture('before').page('http://127.0.0.1:5500/index.html');

test('Finds a logo', async t => {
    await t.expect(Selector('img').getAttribute('src')).contains('cookbook-logo');
});

test('Finds the text visible for non registered user on the page', async t => {
    await t.expect(Selector('h5').innerText).contains('Fehler');
});

test('Finds two visible navigation menu items', async t => {
    await t.expect(Selector('li.logged-out').count).eql(2);
});

test('Failed signs up a new user', async t => {
    // Data fixture
    const email1 = 'probetest.project@gmail.com';
    const pass1 = '123456';

    // Click Sign up
    await t.click(Selector('a[data-target="modal-signup"]'));

    // Fill out the form
    await t.typeText('#signup-email', email1);
    await t.typeText('#signup-password', pass1);
    await t.click(Selector('#signup-form > button'));

    // Catch an error
    await t.expect(Selector('p.error').innerText).contains('The email address is already in use by another account.');
});

test('Successfully signs up a new user', async t => {
    // Data fixture
    const email2 = uuidv4() + '@gmail.com';
    const pass2 = '123456';

    // Click Sign up
    await t.click(Selector('a[data-target="modal-signup"]'));

    // Fill out the form
    await t.typeText('#signup-email', email2);
    await t.typeText('#signup-password', pass2);
    await t.click(Selector('#signup-form > button'));

    //Finds three visible navigations menu
    await t.expect(Selector('li.logged-in').count).eql(3);

    await t.click(Selector('a#logout'));
    await t.expect(Selector('li.logged-out').count).eql(2);
});

test('Log in and create recipe', async t => {
    // Data fixture
    const email1 = 'probetest.project@gmail.com';
    const pass1 = '123456';

    // Click Login
    await t.click(Selector('a[data-target="modal-login"]'));

    // Fill out the form
    await t.typeText('#login-email', email1);
    await t.typeText('#login-password', pass1);
    await t.click(Selector('#login-form > button'));

    //Finds three visible navigations menu
    await t.expect(Selector('li.logged-in').count).eql(3);

    const name = 'Kaffekuchen';
    const ingredients = '250 g Mascarpone, 250 g Löffelbiskuits, 2 Eier, 60 g Puderzucker, 100 ml Espresso, 3 EL Amaretto, 1 EL Schokolade (gerieben), 1 EL Kakaopulver, Salz';
    const cooking = 'Die Eier trennen. Die Eigelbe mit Puderzucker in eine Schüssel geben und cremig schlagen. Anschließend mit Amaretto und der Mascarpone glatt rühren.';

    //Opened a recipe creation menu
    await t.click(Selector('a[data-target="modal-create"]'));

    // Fill out the form
    await t.expect(Selector('#modal-create > div > h4').innerText).contains('Create Recipe');
    await t.typeText('#title', name);
    await t.typeText('#content', ingredients);
    await t.typeText('#direction', cooking);
    await t.click(Selector('#create-form > button'));

    // Finds recipe
    await t.click(Selector('div.collapsible-header').withText('Kaffekuchen'));
    await t.expect(Selector('li.active > div#recipeContent').innerText).contains('250 g Mascarpone');
    await t.expect(Selector('li.active > div#recipeDirection').innerText).contains('Die Eier');

    await t.click(Selector('a#logout'));
    await t.expect(Selector('li.logged-out').count).eql(2);
});

